var moment = require ('moment');
var elasticsearch = require ('elasticsearch');
var shortid = require('shortid');
var config = require('config');

var esClient = new elasticsearch.Client(config.get('elasticsearchClient'));

var elasticsearchController = {};

elasticsearchController.logEvent = function(params, cb) {
  if (params == null || params.id == null) return cb('Invalid request');
  
  elasticsearchController.getGameWithId(params, function(err, game) {
    //console.log(JSON.stringify(params))
    if (err) return cb(err);
    //console.log(JSON.stringify(game));
    if ( game == null || game.length == 0) return cb(null, 'Game does not exist');
    var request = elasticsearchController.buildRequest(params, game);
    //console.log(JSON.stringify(request));
    esClient.index( request, function (error, response) {
     return cb(null, 'Logged event');
    });    
    //Game exists so store log
  });
}

elasticsearchController.buildRequest = function(params, game) {
  var request = {
    index: 'logs',
    type: game._source.name,
    body: {
      event: params.event,
      userId: params.userId,
      sessionId: params.sessionId,
      timestamp: moment().format(),
      levelNumber: params.levelNumber,
      worldNumber: params.worldNumber,
      score: params.score,
      startRating: params.starRating
    }
  };
  return request;
}

//DO NOT EXPOSE TO THE PUBLIC
elasticsearchController.createIndex = function(params) {
  esClient.indices.create({
    index: 'logs',
    _timestamp: { 
      enabled: true,
      stored: true
    }
  },function(err,response,status) {
    if(err) {
      console.log(err);
    }
    else {
      console.log("create",response);
    }
  });

}

elasticsearchController.getGames = function(params, cb) {
  esClient.search({
    index: 'games',
    type: 'game',
    body: {
     query: {
       match: {
         name: params.name
       }
     }
    }   
  }, function (err, response) {
    return cb(err, response.hits.hits);
  });
};

elasticsearchController.getGameWithId = function(params, cb) {
    esClient.search({
    index: 'games',
    type: 'game',
    body: {
     query: {
       match: {
         _id: params.id
       }
     }
    }
  }, function (err, response) {
 
    if (err) return cb(err);
    if (!response || !response.hits || !response.hits.hits[0]) return cb(null, null);
    return cb(err, response.hits.hits[0]);
  });

}

//DO NOT EXPOSE TO PUBLIC
elasticsearchController.addGame = function(params, cb) {
  if (!params.name) return cb(null, 'Must pass in game name');
  elasticsearchController.getGames(params, function (error, response) {
    //MORE VALIDATION
    if (response.length >= 1) {
      return cb(null, 'Game already exists');
    }
    esClient.index({
      index: 'games',
      id: shortid.generate(),
      type: 'game',
      body: {
        name: params.name.trim()
      }
    }, function (error, response) {
     return cb(null, 'Added game');
    });
  });
}

//REALLY DO NOT EXPOSE TO THE PUBLIC
elasticsearchController.deleteIndex = function(params) {

}
module.exports = elasticsearchController;

