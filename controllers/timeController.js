var moment = require ('moment');

var timeController = {};

timeController.getTime = function() {
  return moment().format();
}

module.exports = timeController;