var assert = require('assert');
var sinon = require('sinon');
var moment = require('moment');

var timeController = require('../../../controllers/elasticsearchController');

describe('Elasticsearch Controller', function() {
  describe('logEvent', function() {
    it('Checks with elastic search that the provided game id is accurate', function() {
     
    });

    it ('Pushes up the record assuming that game id is accurate', function() {

    });

    it ('Does not push up a record if the id is not accurate', function() {

    });
  });

  describe('addGame', function() {
    it ('Calls esClient to add a new entry in the index games.', function() {

    });

    it ('Uses shortid to generate a unique short id for that index', function() {

    }); 

    it ('Passes in the string game from params as the body.name for the new game entry index', function() {

    });
  });
});
