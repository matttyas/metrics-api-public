var assert = require('assert');
var sinon = require('sinon');
var moment = require('moment');

var inputController = require('../../../controllers/inputController');

describe('Input Controller', function() {
  describe('processInput', function() {
    it('pretends to log in a user', function(done) {
      var request = {
        query: {
          user: 'matt'
        }
      };

      var expected = {
        user: 'matt',
        id: '1'
      };

      inputController.processInput(request, function(res) {
        assert.deepEqual(res, expected);
        done();
      });
    });
  });
});