var assert = require('assert');
var sinon = require('sinon');
var moment = require('moment');

var timeController = require('../../../controllers/timeController');

describe('Time Controller', function() {
  describe('getTime', function() {
    it('calls moment.format to get the current time and returns that', function() {
     
      clock = sinon.useFakeTimers(new Date(2015,5,26).getTime());
      var expectedTime = '2015-06-26T00:00:00+01:00';
      assert.equal(timeController.getTime(), expectedTime);

      clock.restore();
    });
  });
});