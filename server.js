var Hapi = require('hapi');
var Good = require('good');
var routes = require('./routes');
var server = new Hapi.Server();
var elasticsearch = require('elasticsearch');

server.connection({ port: 3000});

server.register({
  register: Good,
  options: {
    reporters: [{
      reporter: require('good-console'),
      events: {
        response: '*',
        log: '*'
      }
    }]
  }
}, function (err) {
  if (err) {
    throw err;
  }

  server.route(routes);

  server.start(function () {
    server.log ('info', 'Server running at: ', server.info.uri);
  });
});
