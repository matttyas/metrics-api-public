A node API that sits in front of our Metrics system. It acts as the one valid gateway to our Elasticsearch infrastructure.

Installation Instructions
-------------------------

Install npm packages, then install mocha globally to run unit tests:

Create logging directory:

sudo bash install.sh

Starting the api:

You'll need to alter the config to set the IP address of your elastic search server.

nohup node server.js > /var/metrics-api/logs/log 2> /var/metrics-api/logs/err &

Tests
-----

### Unit Tests

To run a single unit test run:

mocha /tests/path/to/test.js

To run all unit tests run the following:

mocha "tests/unit/**/*.js"
