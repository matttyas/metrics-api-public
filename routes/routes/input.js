var inputController = require('../../controllers/inputController');
var joi = require('joi');

module.exports = {
  method: 'GET',
  path: '/input',
  handler: function (request, reply) {
    inputController.processInput(request, reply);
  },
  config: {
    validate: {
      query: {
        name: joi.string().min(3).max(10).required()
      }
    }
  }
}
