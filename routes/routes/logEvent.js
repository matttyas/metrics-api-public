var elasticsearchController = require('../../controllers/elasticsearchController');

module.exports = {
  method: 'GET',
  path: '/logevent',
  handler: function (request, reply) {
    console.log('GETTING DATA');
    //Validate params here
    elasticsearchController.logEvent(request.query, function(err, response) {
       if (err) return reply(err);
       return reply(response);
    });
  }
};
