var timeController = require('../../controllers/timeController');

module.exports = {
  method: 'GET',
  path: '/time',
  handler: function (request, reply) {    
    reply('The time is: ' + timeController.getTime());
  }
};
