//Loads all files in the routes folder as routes for the server

var routes = [];

var normalizedPath = require("path").join(__dirname + '/routes');

require("fs").readdirSync(normalizedPath).forEach(function(file) {
  console.log(file);
  var route = require("./routes/" + file);
  routes = routes.concat(route);
});

//console.log(JSON.stringify(routes, null, 2));

module.exports = routes;
